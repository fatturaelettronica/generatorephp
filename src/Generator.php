<?php

namespace FatturaElettronica;

use Sabre\Xml\Writer;

class Generator
{

    public $xml = null;

    public $data = null;

    
    public function __construct($file)
    {
        $this->xml = new Writer();
        $this->xml->openUri($file);
        $this->xml->setIndent(true);
        $this->xml->startDocument();
    }

    public function setData(array $data)
    {
        //TODO setData from a given array
    }

    public function execute()
    {
        $this->xml->endDocument();
        $this->xml->flush();
    }
}